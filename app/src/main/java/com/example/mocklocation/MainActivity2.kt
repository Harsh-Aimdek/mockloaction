package com.example.mocklocation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.example.mocklocation.mockcheck.MockCheck

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnMock = findViewById<AppCompatButton>(R.id.btnCheckMock)
        btnMock.setOnClickListener {
            Toast.makeText(this, MockCheck(this).isMockedLocation().toString(), Toast.LENGTH_SHORT)
                .show()
//            MockCheck(this).isMockedLocation()
        }
        MockCheck(this).observerMockChanges().observe(this) {
            Toast.makeText(this, "Mocked location : $it", Toast.LENGTH_SHORT).show()
        }
    }
}